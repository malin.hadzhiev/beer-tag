package com.module3.beertag.services;

import com.module3.beertag.models.Brewery;
import com.module3.beertag.models.Rating;
import com.module3.beertag.repositories.contracts.BreweryRepository;
import com.module3.beertag.repositories.contracts.RatingRepository;
import com.module3.beertag.services.contracts.RatingService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class RatingServiceImplTest {

        @Mock
        RatingRepository ratingRepository;

        @InjectMocks
        RatingServiceImpl ratingService;

        private Rating mockRating = new Rating();

        @Test
        public void get_All_Should_Return_A_List_Of_Ratings() {
            //Arrange
            Mockito.when(ratingRepository.getAll())
                    .thenReturn(Collections.singletonList(mockRating));
            //Act
            List<Rating> ratings = ratingService.getAll();
            //Assert
            Assert.assertEquals(1, ratings.size());
        }

    @Test
    public void getById_Should_ReturnRightRating_WhenExist() {
        Mockito.when(ratingRepository.getRatingById(1))
                .thenReturn(new Rating());

        Rating ratingResult = ratingService.getRatingById(1);
        assertEquals(0, ratingResult.getId());
    }

    @Test
    public void create_Should_Call_Repository_Create() {
        Rating rating = new Rating();
        ratingService.createRating(rating);
        Mockito.verify(ratingRepository, Mockito.times(1))
                .create(rating);
    }
}
