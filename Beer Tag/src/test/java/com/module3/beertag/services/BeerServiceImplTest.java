package com.module3.beertag.services;

import com.module3.beertag.models.*;
import com.module3.beertag.repositories.BeerRepositoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class BeerServiceImplTest {

    private List<Beer> beers;

    @Mock
    BeerRepositoryImpl beerRepository;

    @InjectMocks
    BeerServiceImpl beerService;

    private Style dark = new Style("dark");
    private Style bitter = new Style("bitter");
    private Country bg = new Country("Bulgaria");
    private Country en = new Country("England");
    private Brewery br = new Brewery("br");

    @Before
    public void setUp() {
        beers = new ArrayList<>();
        beers.add(new Beer("Pirinsko", 6));
        beers.add(new Beer("Ariana", 3));
    }

    @Test
    public void getById_Should_ReturnRightBeer_WhenExist() {
        Mockito.when(beerRepository.getById(1))
                .thenReturn(new Beer("Zagorka", 2));

        Beer beerResult = beerService.getById(1);
        assertEquals(0, beerResult.getId());
    }

    @Test
    public void getByName_Should_ReturnRightBeer_WhenExist() {
        Mockito.when(beerRepository.getByName("Zagorka"))
                .thenReturn(new Beer("Zagorka", 2));

        Beer beerResult = beerService.getByName("Zagorka");
        assertEquals("Zagorka", beerResult.getName());
    }

    @Test
    public void getAllBeers_Should_ReturnAllBeers() {
        // Arrange
        Mockito.when(beerService.getAll()).thenReturn(helperCreateNewBeers());

        // Act
        List<Beer> result = beerService.getAll();

        // Assert
        Assert.assertEquals(3, result.size());
    }

    @Test
    public void create_Should_CallRepositoryCreate_When_CreatingNewBeer() {
        // Arrange
        Beer beer = new Beer("TestName", 5.4);

        // Act
        beerService.createBeer(beer);

        // Assert
        Mockito.verify(beerRepository, Mockito.times(1)).createBeer(beer);
    }

    @Test
    public void update_Should_CallRepositoryUpdate_When_UpdatingNewBeer() {
        // Arrange
        Beer beer = new Beer("TestName", 5.4);

        // Act
        beerService.updateBeer(1, beer);

        // Assert
        Mockito.verify(beerRepository, Mockito.times(1)).updateBeer(1, beer);
    }

    @Test
    public void sortByAlphabetically_Should_ReturnAllBeersSortedByAlphabetically() {
        Mockito.when(beerRepository.sortAlphabetically()).thenReturn(beers);

        assertEquals(2, beerService.sortAlphabetically().size());
    }

    @Test
    public void sortByAbv_Should_ReturnAllBeersSortedByAbv() {
        Mockito.when(beerRepository.sortedByABV()).thenReturn(beers);

        assertEquals(2, beerService.sortByABV().size());
    }

    @Test
    public void sortByRating_Should_ReturnAllBeersSortedByRating() {
        Mockito.when(beerRepository.sortedByRating()).thenReturn(beers);

        assertEquals(2, beerService.sortByRating().size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void filter_Should_ShowResults_When_StyleExist() {
        // Arrange
        Mockito.when(beerRepository.getAll())
                .thenReturn(Arrays.asList(
                        new Beer(1, "Pirinsko", "Pivovr", "Bulgaria", 3.9, "Chisto ot planinata", 6, "dark"),
                        new Beer(4, "Heineken", "Nemska Pivovarna Sofia", "Bulgaria", 3.9, "Chisto ot Germaniq", 6, "dark"),
                        new Beer(5, "Zagorka", "Heineken", "Bulgaria", 4, "Chisto ot nqkude", 2, "dark")
                ));

        //Act
        List<Beer> result = beerService.getAllFilteredByStyle("dark");

        // Assert
        Assert.assertEquals(beerService.getAllFilteredByStyle("dark"), result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void filter_Should_ShowResults_When_CountryExist() {
        // Arrange
        Mockito.when(beerRepository.getAll())
                .thenReturn(Arrays.asList(
                        new Beer(1, "Pirinsko", "Pivovr", "Bulgaria", 3.9, "Chisto ot planinata", 6, "dark"),
                        new Beer(4, "Heineken", "Nemska Pivovarna Sofia", "Bulgaria", 3.9, "Chisto ot Germaniq", 6, "dark"),
                        new Beer(5, "Zagorka", "Heineken", "Bulgaria", 4, "Chisto ot nqkude", 2, "dark")
                ));

        //Act
        List<Beer> result = beerService.getAllFilteredByCountry("Bulgaria");

        // Assert
        Assert.assertEquals(beerService.getAllFilteredByCountry("Bulgaria"), result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void filter_Should_ShowResults_When_TagExist() {
        //Act
        List<Beer> result = beerService.getAllFilteredByTag("fresh");

        // Assert
        Assert.assertEquals(beerService.getAllFilteredByTag("fresh"), result);
    }

    @Test
    public void delete_Should_Call_Repository_Delete() {
        beerService.deleteBeer(1);
        Mockito.verify(beerRepository, Mockito.times(1)).deleteBeer(1);
    }

    @Test
    public void getCountries_Should_ReturnCountries() {
        // Arrange
        Mockito.when(beerRepository.countries())
                .thenReturn(Arrays.asList(
                        new Country(),
                        new Country(),
                        new Country()
                ));

        //Act
        List<Country> result = beerService.countries();

        // Assert
        Assert.assertEquals(beerRepository.countries(), result);
    }

    @Test
    public void getBeerTags_Should_ReturnTags() {
        // Arrange
        Mockito.when(beerRepository.getBeerTags(1))
                .thenReturn(Arrays.asList(
                        new Tag(),
                        new Tag(),
                        new Tag()
                ));

        //Act
        List<Tag> result = beerService.getBeerTags(1);

        // Assert
        Assert.assertEquals(beerRepository.getBeerTags(1), result);
    }

//    public void getBeerRatings_Should_ReturnRatings() {
//        // Arrange
//        Mockito.when(beerRepository.getBeerRatingList(1))
//                .thenReturn(Arrays.asList(
//                        new Rating(),
//                        new Rating(),
//                        new Rating()
//                ));
//
//        //Act
//        List<Rating> result = beerService.getBeerRatingList(1);
//
//        // Assert
//        Assert.assertEquals(beerRepository.getBeerRatingList(1), result);
//    }

    private List<Beer> helperCreateNewBeers() {
        return Arrays.asList(
                new Beer("ATestName1", br, bg, 5.4, dark),
                new Beer("BTestName2", br, en, 5.2, bitter),
                new Beer("CTestName3", br, bg, 5.1, dark)
        );
    }
}
