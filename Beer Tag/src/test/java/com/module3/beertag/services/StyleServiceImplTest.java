package com.module3.beertag.services;

import com.module3.beertag.models.Country;
import com.module3.beertag.models.Style;
import com.module3.beertag.repositories.contracts.CountryRepository;
import com.module3.beertag.repositories.contracts.StyleRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class StyleServiceImplTest {
    @Mock
    StyleRepository styleRepository;

    @InjectMocks
    StyleServiceImpl styleService;

    private Style mockStyle = new Style("bulgarian");

    @Test
    public void get_All_Should_Return_A_List_Of_Styles() {
        //Arrange
        Mockito.when(styleRepository.getAll())
                .thenReturn(Collections.singletonList(mockStyle));
        //Act
        List<Style> countries = styleService.getAll();
        //Assert
        Assert.assertEquals(1, countries.size());
    }
}
