package com.module3.beertag.services;

import com.module3.beertag.models.*;
import com.module3.beertag.repositories.UserRepositoryImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    @Mock
    UserRepositoryImpl userRepository;

    @InjectMocks
    UserServiceImpl userService;

    private BeerStatus beerStatus = new BeerStatus();


    @Test
    public void getAllUsers_Should_ReturnAllUsers() {
        // Arrange
        Mockito.when(userRepository.getAll()).thenReturn(Arrays.asList(
                new User("TestLastName1", "12345"),
                new User("TestLastName2", "12345"),
                new User("TestLastName3", "12345")
        ));

        // Act
        List<User> result = userService.getAll();

        // Assert
        Assert.assertEquals(3, result.size());
    }

    @Test
    public void create_Should_CallRepository_When_Create_NewUser() {
        // Arrange
        User user = new User("testUsername1", "12345");

        // Act
        userService.create(user);

        // Assert
        Mockito.verify(userRepository, Mockito.times(1))
                .create(user);
    }

    @Test
    public void getByUsername_Should_ReturnUser_When_User_Exist() {
        // Arrange
        User user = new User("testUsername1", "12345");

        Mockito.when(userRepository.getByUsername("testUsername1")).thenReturn(user);

        // Act
        User result = userService.getUserByName("testUsername1");

        // Assert
        Assert.assertEquals(user, result);

    }

    @Test
    public void add_Beer_AsWish_By_User() {
        // Arrange
        User user = new User("testUsername1", "12345");
        Beer beer = new Beer("TestBrewery", 5.4);
        user.setId(1);
        beer.setId(1);

        int userId = user.getId();
        int beerId = beer.getId();

        // Act
        userService.addToWishList(userId, beerId);

        // Assert
        Mockito.verify(userRepository, Mockito.times(1))
                .addToWishList(userId, beerId);
    }

    @Test
    public void add_Beer_AsDrank_By_User() {
        // Arrange
        User user = new User("testUsername1", "12345");
        Beer beer = new Beer("TestBrewery", 5.4);
        user.setId(1);
        beer.setId(1);

        int userId = user.getId();
        int beerId = beer.getId();

        // Act
        userService.addToDrankList(userId, beerId);

        // Assert
        Mockito.verify(userRepository, Mockito.times(1))
                .addToDrankList(userId, beerId);
    }

    @Test
    public void addToWantToDrinkList_Should_Call_The_User_repository(){
        Mockito.when(userRepository.getUserBeerStatus(1))
                .thenReturn(Arrays.asList(beerStatus));

        userService.getUserWantToDrinkStatus( 1);

        Mockito.verify(userRepository, Mockito.times(1)).getUserBeerStatus( 1);
    }

    @Test
    public void getUserBeerStatus_Should_ReturnBeerStatuses() {
        // Arrange
        Mockito.when(userRepository.getUserBeerStatus(1))
                .thenReturn(Arrays.asList(
                        new BeerStatus(),
                        new BeerStatus(),
                        new BeerStatus()
                ));

        //Act
        List<BeerStatus> result = userService.getUserBeerStatus(1);

        // Assert
        Assert.assertEquals(userRepository.getUserBeerStatus(1), result);
    }

    @Test
    public void getUserDrunkStatus_Should_ReturnBeerStatuses() {
        //Act
        List<BeerStatus> result = userService.getUserDrunkStatus(1);

        // Assert
        Assert.assertEquals(userRepository.getUserBeerStatus(1), result);
    }

    @Test
    public void getUserCreatedBeers_Should_ReturnBeers() {
        // Arrange
        Mockito.when(userRepository.getUserCreatedBeers(1))
                .thenReturn(Arrays.asList(
                        new Beer(),
                        new Beer(),
                        new Beer()
                ));

        //Act
        List<Beer> result = userService.getUserCreatedBeers(1);

        // Assert
        Assert.assertEquals(userRepository.getUserCreatedBeers(1), result);
    }

    @Test
    public void getUserRatedBeers_Should_ReturnRatedBeers() {
        // Arrange
        Mockito.when(userRepository.getUserRatedBeers(1))
                .thenReturn(Arrays.asList(
                        new Rating(),
                        new Rating(),
                        new Rating()
                ));

        //Act
        List<Rating> result = userService.getUserRatedBeers(1);

        // Assert
        Assert.assertEquals(userRepository.getUserRatedBeers(1), result);
    }

    @Test
    public void getUserTopRatedBeers_Should_ReturnRatedBeers() {
        // Arrange
        Mockito.when(userRepository.getUserRatedBeers(1))
                .thenReturn(Arrays.asList(
                        new Rating(),
                        new Rating(),
                        new Rating()
                ));

        //Act
        List<Rating> result = userService.getUserTopRatedBeers(1);

        // Assert
        Assert.assertEquals(userRepository.getUserRatedBeers(1), result);
    }

//    @Test
//    public void addRatingToBeer_As_User() {
//        // Arrange
//        User user = new User();
//        Beer beer = new Beer("TestName", 5.4);
//        user.setId(1);
//        beer.setId(1);
//
//        int userId = user.getId();
//        int beerId = beer.getId();
//        int rating = 5;
//
//        // Act
//        userService.addRating(rating, userId, beerId);
//
//        // Assert
//        Mockito.verify(userRepository, Mockito.times(1))
//                .addRating(rating, userId, beerId);
//    }
}
