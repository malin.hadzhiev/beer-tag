package com.module3.beertag.services;

import com.module3.beertag.models.Brewery;
import com.module3.beertag.repositories.contracts.BreweryRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class BreweryServiceImplTest {
    @Mock
    BreweryRepository breweryRepository;

    @InjectMocks
    BreweryServiceImpl breweryService;

    private Brewery mockBrewery = new Brewery("Divo Pivo");

    @Test
    public void get_All_Should_Return_A_List_Of_Breweries() {
        //Arrange
        Mockito.when(breweryRepository.getAll())
                .thenReturn(Collections.singletonList(mockBrewery));
        //Act
        List<Brewery> breweries = breweryService.getAll();
        //Assert
        Assert.assertEquals(1, breweries.size());
    }

    @Test
    public void create_Should_Call_Repository_Create() {
        Brewery brewery = new Brewery();
        breweryService.createBrewery(brewery);
        Mockito.verify(breweryRepository, Mockito.times(1))
                .create(brewery);
    }

    @Test
    public void getById_Should_ReturnRightBrewery_WhenExist() {
        Mockito.when(breweryRepository.getBreweryById(1))
                .thenReturn(new Brewery(1, "Divo Pivo"));

        Brewery breweryResult = breweryService.getBreweryById(1);
        assertEquals(1, breweryResult.getId());
    }

    @Test
    public void getByName_Should_ReturnRightBrewery_WhenExist() {
        Mockito.when(breweryRepository.getBreweryByName("Divo Pivo"))
                .thenReturn(new Brewery(1, "Divo Pivo"));

        Brewery breweryResult = breweryService.getBreweryByName("Divo Pivo");
        assertEquals("Divo Pivo", breweryResult.getBrewery());
    }

    @Test(expected = IllegalArgumentException.class)
    public void get_Tag_create_Should_Return_An_Exception_When_Brewery_Exists() {
        //Arrange
        Mockito.when(breweryRepository.getAll())
                .thenReturn(Collections.singletonList(mockBrewery));

        //Act
        breweryService.createBrewery(mockBrewery);

        //Assert
        Mockito.verify(breweryRepository, Mockito.times(0)).create(mockBrewery);
    }

    @Test
    public void update_Should_Call_Repository_Update() {
        Brewery brewery = new Brewery();
        breweryService.updateBrewery(1, brewery);
        Mockito.verify(breweryRepository, Mockito.times(1)).updateBrewery(1, brewery);
    }

    @Test
    public void delete_Should_Call_Repository_Delete() {
        Brewery brewery = new Brewery(1, "asd");
        Mockito.when(breweryRepository.getBreweryById(1))
                .thenReturn(brewery);

        breweryService.deleteBrewery(1);
        Mockito.verify(breweryRepository, Mockito.times(0))
                .deleteBrewery(1);
    }
}

