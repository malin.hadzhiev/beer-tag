package com.module3.beertag.services;

import com.module3.beertag.models.Tag;
import com.module3.beertag.repositories.TagRepositoryImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTest {
        @Mock
        TagRepositoryImpl tagRepository;

        @InjectMocks
        TagServiceImpl tagService;

        private Tag mockTag = new Tag("fresh");

        @Test
        public void get_All_Should_Return_A_List_Of_Tags() {
            //Arrange
            Mockito.when(tagRepository.getAll())
                    .thenReturn(Collections.singletonList(mockTag));
            //Act
            List<Tag> tags = tagService.getAll();
            //Assert
            Assert.assertEquals(1, tags.size());
        }

        @Test
        public void create_Should_Call_Repository_Create() {
            Tag tag = new Tag();
            tagService.createTag(tag);
            Mockito.verify(tagRepository, Mockito.times(1))
                    .create(tag);
        }

        @Test
        public void getById_Should_ReturnRightTag_WhenExist() {
            Mockito.when(tagRepository.getTagById(1))
                    .thenReturn(new Tag("fresh"));

            Tag tagResult = tagService.getTagById(1);
            assertEquals(0, tagResult.getId());
        }

        @Test
        public void getByName_Should_ReturnRightTag_WhenExist() {
            Mockito.when(tagRepository.getTagByName("fresh"))
                    .thenReturn(new Tag( "fresh"));

            Tag tagResult = tagService.getTagByName("fresh");
            assertEquals("fresh", tagResult.getTag());
        }

        @Test(expected = IllegalArgumentException.class)
        public void get_Tag_create_Should_Return_An_Exception_When_Tag_Exists() {
            //Arrange
            Mockito.when(tagRepository.getAll())
                    .thenReturn(Collections.singletonList(mockTag));

            //Act
            tagService.createTag(mockTag);

            //Assert
            Mockito.verify(tagRepository, Mockito.times(0)).create(mockTag);
        }

        @Test
        public void update_Should_Call_Repository_Update() {
            Tag tag = new Tag();
            tagService.updateTag(1, tag);
            Mockito.verify(tagRepository, Mockito.times(1)).updateTag(1, tag);
        }

    @Test
    public void delete_Should_Call_Repository_Delete() {
        Tag tag = new Tag("asd");
        Mockito.when(tagRepository.getTagById(1))
                .thenReturn(tag);

        tagService.deleteTag(1);
        Mockito.verify(tagRepository, Mockito.times(0))
                .deleteTag(1);
    }
}
