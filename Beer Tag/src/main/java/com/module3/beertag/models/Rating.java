package com.module3.beertag.models;

import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;

@Entity
@Table(name = "ratings")
public class Rating {
    @PositiveOrZero
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @PositiveOrZero
    @Column(name = "rating")
    private double rating;

   @ManyToOne
   @JoinColumn(name = "beer_id")
   private Beer beer;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Rating(int rating,Beer beer) {
        this.rating = rating;
        this.beer = beer;
    }

    public Rating(){

    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Beer getBeer() {
        return beer;
    }

    public void setBeer(Beer beer) {
        this.beer = beer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}
