package com.module3.beertag.models;

import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;



@Entity
@Table(name = "users_to_beers")
public class BeerStatus {
    @PositiveOrZero
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;


    @Column(name = "is_drunk", columnDefinition = "BOOLEAN")
    private boolean status;

    @ManyToOne
    @JoinColumn(name = "beer_id")
    private Beer beer;

    public BeerStatus(boolean status, Beer beer) {
        this.status = status;
        this.beer = beer;
    }

    public BeerStatus() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Beer getBeer() {
        return beer;
    }

    public void setBeer(Beer beer) {
        this.beer = beer;
    }
}
