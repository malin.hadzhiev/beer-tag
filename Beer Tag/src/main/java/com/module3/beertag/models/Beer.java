package com.module3.beertag.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "beers")
public class Beer{
    @PositiveOrZero
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Size(min = 2, max = 20)
    @Column(name = "name")
    private String name;

    @PositiveOrZero
    @Column(name = "ABV")
    private double ABV;

    @Size(min = 2, max = 500)
    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "brewery_id")
    private Brewery brewery;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;

    @ManyToOne
    @JoinColumn(name = "style_id")
    private Style style;

    @JsonIgnore
    @ManyToMany
    @JoinTable(
            name = "beers_to_tags",
            joinColumns = @JoinColumn(name = "beer_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private List<Tag> tags;

    @JsonIgnore
    @OneToMany
    @JoinTable(
            name = "ratings",
            joinColumns = @JoinColumn(name = "beer_id"),
            inverseJoinColumns = @JoinColumn(name = "id")
    )
    private List<Rating> ratingList;

    @Column(name = "average_rating")
    private double averageRating;

    @ManyToOne
    @JoinColumn(name = "creator_id")
    private User creator;

    @Column(name = "picture")
    private String picture;


    public Beer(@Size(min = 2, max = 20) String name, @PositiveOrZero double ABV) {
        this.name = name;
        this.ABV = ABV;
    }

    public Beer(@Size(min = 2, max = 20) String name, Brewery brewery, Country country, @PositiveOrZero double ABV, Style style) {
        this.name = name;
        this.brewery = brewery;
        this.country = country;
        this.ABV = ABV;
        this.style = style;
    }

    public Beer(int id, String name, String brewery, String country, double ABV, String tag, int rating, String style) {

    }

    public double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(double averageRating) {
        this.averageRating = averageRating;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Beer() {
    }

    public List<Rating> getRatingList() {
        return ratingList;
    }

    public void setRatingList(List<Rating> ratingList) {
        this.ratingList = ratingList;
    }

    public Brewery getBrewery() {
        return brewery;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public void setBrewery(Brewery brewery) {
        this.brewery = brewery;
    }

    public double getABV() {
        return ABV;
    }

    public void setABV(double ABV) {
        this.ABV = ABV;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void addTag(Tag tag) {
        tags.add(tag);
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public String toString() {
        return  name;
    }
}
