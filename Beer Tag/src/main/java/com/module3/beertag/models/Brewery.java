package com.module3.beertag.models;

import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

@Entity
@Table(name = "breweries")
public class Brewery {
    @PositiveOrZero
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Size(min = 2, max = 20)
    @Column(name = "name")
    private String brewery;

    public Brewery(int id, String name) {
        this.id = id;
        this.brewery = name;
    }

    public Brewery(@Size(min = 2, max = 20) String brewery) {
        this.brewery = brewery;
    }

    public Brewery() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrewery() {
        return brewery;
    }

    public void setBrewery(String brewery) {
        this.brewery = brewery;
    }
}
