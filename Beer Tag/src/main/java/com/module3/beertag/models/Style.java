package com.module3.beertag.models;

import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

@Entity
@Table(name = "styles")
public class Style {
    @PositiveOrZero
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Size(min = 2, max = 20)
    @Column(name = "name")
    private String style;

    public Style(int id, String name) {
        this.id = id;
        this.style = name;
    }

    public Style(@Size(min = 2, max = 20) String style) {
        this.style = style;
    }

    public Style() {
        this(0,"");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
}
