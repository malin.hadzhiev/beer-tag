package com.module3.beertag.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "tags")
public class Tag {
    @PositiveOrZero
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Size(min = 2, max = 20)
    @Column(name = "name")
    private String tag;

    @JsonIgnore
    @ManyToMany
    @JoinTable(
            name = "beers_to_tags",
            joinColumns = @JoinColumn(name = "tag_id"),
            inverseJoinColumns = @JoinColumn(name = "beer_id")
    )
    private List<Beer> beers;

    public Tag(String tag){
        this.tag = tag;
        beers = new ArrayList<>();
    }

    public Tag() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

   public List<Beer> getBeers() {
       return beers;
   }

   public void setBeers(List<Beer> beers) {
       this.beers = beers;
   }

    public int getId() {
        return id;
    }
}
