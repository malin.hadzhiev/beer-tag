package com.module3.beertag.models;

import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "users")
public class User {
    @PositiveOrZero
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;


    @Size(min = 2, max = 20, message = "Username is required")
    @Column(name = "username")
    private String username;


    @Size(min = 2, max = 20, message = "Password is required")
    @Column(name = "password")
    private String password;

    //private String passwordConfirmation;

    @OneToMany
    @JoinTable(
            name = "ratings",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "id")
    )
    private List<Rating> ratedBeers;

    @OneToMany
    @JoinTable(
            name = "users_to_beers",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "id")
    )
    private List<BeerStatus> beerStatusList;



    @OneToMany
    @JoinColumn(name = "creator_id")
    private List<Beer> createdBeers;

    public User(int id, @Size(min = 2, max = 20) String username, @Size(min = 2, max = 20) String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public User(@Size(min = 2, max = 20, message = "Username is required") String username, @Size(min = 2, max = 20, message = "Password is required") String password) {
        this.username = username;
        this.password = password;
    }

    public User() {
    }

    public List<BeerStatus> getBeerStatusList() {
        return beerStatusList;
    }

    public void setBeerStatusList(List<BeerStatus> beerStatusList) {
        this.beerStatusList = beerStatusList;
    }

    public List<Rating> getRatedBeers() {
        return ratedBeers;
    }

    public void setRatedBeers(List<Rating> ratedBeers) {
        this.ratedBeers = ratedBeers;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Beer> getCreatedBeers() {
        return createdBeers;
    }

    public void setCreatedBeers(List<Beer> createdBeers) {
        this.createdBeers = createdBeers;
    }

    //    public String getPasswordConfirmation() {
//        return passwordConfirmation;
//    }
//
//    public void setPasswordConfirmation(String passwordConfirmation) {
//        this.passwordConfirmation = passwordConfirmation;
//    }
}
