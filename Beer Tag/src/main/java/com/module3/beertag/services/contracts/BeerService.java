package com.module3.beertag.services.contracts;

import com.module3.beertag.models.Beer;
import com.module3.beertag.models.Country;
import com.module3.beertag.models.Rating;
import com.module3.beertag.models.Tag;

import java.util.List;

public interface BeerService {
    List<Beer> getAll();

    void createBeer(Beer beer);

    List<Beer> sortAlphabetically();

    List<Beer> sortByABV();

    List<Beer> sortByRating();

    List<Tag> getBeerTags(int id);

    List<Rating> getBeerRatingList(int id);

    void addTagToBeer(String tag, int beerId);

    List<Country> countries();

    Beer getByName(String name);

    Beer getById(int id);

    void updateBeer(int id, Beer beer);

    void deleteBeer(int id);

    List<Beer> getAllFilteredByStyle(String styleName);

    List<Beer> getAllFilteredByCountry(String countryName);

    List<Beer> getAllFilteredByTag(String tagName);
}
