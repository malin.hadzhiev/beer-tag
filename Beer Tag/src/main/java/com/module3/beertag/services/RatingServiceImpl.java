package com.module3.beertag.services;

import com.module3.beertag.models.Rating;
import com.module3.beertag.repositories.contracts.RatingRepository;
import com.module3.beertag.services.contracts.RatingService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RatingServiceImpl implements RatingService {
    private RatingRepository ratingRepository;

    public RatingServiceImpl(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    }

    @Override
    public List<Rating> getAll() {
        return ratingRepository.getAll();
    }

    @Override
    public Rating getRatingById(int id) {
        return ratingRepository.getRatingById(id);
    }

    @Override
    public void createRating(Rating rating) {
        ratingRepository.create(rating);
    }
}
