package com.module3.beertag.services.contracts;

import com.module3.beertag.models.Brewery;

import java.util.List;

public interface BreweryService {
    Brewery getBreweryById(int id);

    Brewery getBreweryByName(String name);

    List<Brewery> getAll();

    void createBrewery(Brewery brewery);

    void updateBrewery(int id, Brewery brewery);

    void deleteBrewery(int id);
}
