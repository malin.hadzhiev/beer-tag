package com.module3.beertag.services;

import com.module3.beertag.models.Brewery;
import com.module3.beertag.repositories.contracts.BreweryRepository;
import com.module3.beertag.services.contracts.BreweryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BreweryServiceImpl implements BreweryService {
    private BreweryRepository breweryRepository;

    @Autowired
    public BreweryServiceImpl(BreweryRepository breweryRepository) {
        this.breweryRepository = breweryRepository;
    }

    @Override
    public Brewery getBreweryById(int id) {
        return breweryRepository.getBreweryById(id);
    }

    @Override
    public Brewery getBreweryByName(String name) {
        return breweryRepository.getBreweryByName(name);
    }

    @Override
    public List<Brewery> getAll() {
        return breweryRepository.getAll();
    }

    @Override
    public void createBrewery(Brewery brewery) {
        List<Brewery> countries = breweryRepository.getAll().stream()
                .filter(b -> b.getBrewery().equals(brewery.getBrewery()))
                .collect(Collectors.toList());
        if(countries.size() > 0) {
            throw new IllegalArgumentException(
                    String.format("Brewery with name %s already exists.", brewery.getBrewery()));
        }
        breweryRepository.create(brewery);
    }

    @Override
    public void updateBrewery(int id, Brewery brewery) {
        breweryRepository.updateBrewery(id, brewery);
    }

    @Override
    public void deleteBrewery(int id) {
        Brewery brewery = breweryRepository.getBreweryById(id);
        if (brewery == null) {
            throw new IllegalArgumentException(
                    String.format("Brewery with id %d does not exists!", id));
        }
    }
}
