package com.module3.beertag.services.contracts;

import com.module3.beertag.models.Tag;

import java.util.List;

public interface TagService {
    Tag getTagById(int tagId);

    Tag getTagByName(String name);

    List<Tag> getAll();

    void createTag(Tag tag);

    void updateTag(int id, Tag tag);

    void deleteTag(int id);
}
