package com.module3.beertag.services;

import com.module3.beertag.models.Tag;
import com.module3.beertag.repositories.contracts.TagRepository;
import com.module3.beertag.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TagServiceImpl implements TagService {
    private TagRepository tagRepository;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }


    @Override
    public Tag getTagById(int tagId) {
        Tag tag = tagRepository.getTagById(tagId);
        if (tag == null) {
            throw new IllegalArgumentException(String.format("Tag with %d doesn't exist", tagId));
        }
        return tagRepository.getTagById(tagId);
    }

    @Override
    public Tag getTagByName(String name) {
        return tagRepository.getTagByName(name);
    }

    @Override
    public List<Tag> getAll() {
        return tagRepository.getAll();
    }

    @Override
    public void createTag(Tag tag) {
        String name = tag.getTag();

        List<Tag> alreadyExist =
                tagRepository
                        .getAll()
                        .stream()
                        .filter(u -> u.getTag().equals(name))
                        .collect(Collectors.toList());

        if (!alreadyExist.isEmpty()) {
            throw new IllegalArgumentException(
                    String.format("Tag with name %s already exists", name));
        }
        tagRepository.create(tag);
    }

    @Override
    public void updateTag(int id, Tag tag) {
        tagRepository.updateTag(id, tag);
    }

    @Override
    public void deleteTag(int id) {
        Tag tag = tagRepository.getTagById(id);
        if (tag == null) {
            throw new IllegalArgumentException(
                    String.format("Tag with id %d does not exists!", id));
        }
    }
}
