package com.module3.beertag.services.contracts;

import com.module3.beertag.models.Beer;
import com.module3.beertag.models.Rating;
import com.module3.beertag.models.User;
import com.module3.beertag.models.BeerStatus;

import java.util.List;

public interface UserService {
    List<User> getAll();

    List<Rating> getUserRatedBeers(int id);

    List<Beer> getUserCreatedBeers(int id);

    List<BeerStatus> getUserDrunkStatus(int id);

    List<BeerStatus> getUserWantToDrinkStatus(int id);

    String create(User user);

    User getUserByName(String username);

    List<BeerStatus> getUserBeerStatus(int id);

    void addToDrankList(int userId, int beerId);

    void addToWishList(int userId, int beerId);

    void addRating(int rating, int userId, int beerId);

    List<Rating> getUserTopRatedBeers(int id);
}
