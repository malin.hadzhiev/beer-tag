package com.module3.beertag.services;

import com.module3.beertag.models.Beer;
import com.module3.beertag.models.BeerStatus;
import com.module3.beertag.models.Rating;
import com.module3.beertag.models.User;
import com.module3.beertag.repositories.contracts.BeerRepository;
import com.module3.beertag.repositories.contracts.UserRepository;
import com.module3.beertag.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;
    private BeerRepository beerRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, BeerRepository beerRepository) {
        this.userRepository = userRepository;
        this.beerRepository = beerRepository;
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public List<Rating> getUserRatedBeers(int id) {
        return userRepository.getUserRatedBeers(id);
    }

    @Override
    public List<Beer> getUserCreatedBeers(int id) {
        return userRepository.getUserCreatedBeers(id);
    }

    @Override
    public List<BeerStatus> getUserDrunkStatus(int id) {
        return userRepository.getUserBeerStatus(id)
                .stream()
                .filter(BeerStatus::isStatus)
                .collect(Collectors.toList());
    }

    @Override
    public List<BeerStatus> getUserWantToDrinkStatus(int id) {
        return userRepository.getUserBeerStatus(id)
                .stream()
                .filter(p -> !p.isStatus())
                .collect(Collectors.toList());
    }
    public List<BeerStatus> getUserBeerStatus(int id) {
        return userRepository.getUserBeerStatus(id);
    }

    @Override
    public User getUserByName(String username) {
        User user = userRepository.getByUsername(username);
        if (user == null) {
            throw new IllegalArgumentException(String.format("user with name %s not found.", username));
        }
        return user;
    }


    @Override
    public String create(User user) {
        User isExist = userRepository.getByUsername(user.getUsername());
        if (isExist != null) {
            throw new IllegalArgumentException(
                    String.format("User with name %s already exists", user.getUsername()));
        }

        userRepository.create(user);
        return "Successfully created";
    }
    @Override
    public void addToDrankList(int userId, int beerId) {
        BeerStatus status = getUserBeerStatus(userId)
                .stream()
                .filter(p -> p.getBeer().getId() == beerId)
                .findAny()
                .orElse(null);
        if (status == null) {
            userRepository.addToDrankList(userId, beerId);
        } else {
            userRepository.updateToDrankList(userId,beerId,status.getId());
        }
    }
    @Override
    public void addToWishList(int userId, int beerId) {
        BeerStatus status = getUserBeerStatus(userId)
                .stream()
                .filter(p -> p.getBeer().getId() == beerId)
                .findAny()
                .orElse(null);
        if (status == null) {
            userRepository.addToWishList(userId, beerId);
        }
    }

    @Override
    public void addRating(int rating, int userId, int beerId){
        Rating ratingExists = userRepository.getUserRatedBeers(userId).stream().filter(p -> p.getBeer().getId() == beerId).findAny().orElse(null);
        if (ratingExists == null){
            userRepository.addRating(rating, userId, beerId);
        }else {
            userRepository.editRating(rating,userId,beerId);
        }
        if (!beerRepository.getBeerRatingList(beerId).isEmpty()){
            double averageRating = beerRepository.getBeerRatingList(beerId).stream().mapToDouble(Rating::getRating).average().getAsDouble();
            beerRepository.updateAverageRating(beerId,averageRating);
        }
    }
    @Override
    public List<Rating> getUserTopRatedBeers(int id) {
        List<Rating> topRatedBeers = userRepository.getUserRatedBeers(id).stream()
                .sorted(Comparator.comparing(Rating::getRating).reversed()).limit(3)
                .collect(Collectors.toList());
        return topRatedBeers;
    }
}
