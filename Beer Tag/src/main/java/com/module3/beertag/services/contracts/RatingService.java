package com.module3.beertag.services.contracts;

import com.module3.beertag.models.Rating;

import java.util.List;

public interface RatingService {
    List<Rating> getAll();

    Rating getRatingById(int id);

    void createRating(Rating rating);
}
