package com.module3.beertag.services;

import com.module3.beertag.models.*;
import com.module3.beertag.repositories.contracts.BeerRepository;
import com.module3.beertag.repositories.contracts.CountryRepository;
import com.module3.beertag.repositories.contracts.StyleRepository;
import com.module3.beertag.repositories.contracts.TagRepository;
import com.module3.beertag.services.contracts.BeerService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BeerServiceImpl implements BeerService {
    private BeerRepository beerRepository;
    private TagRepository tagRepository;
    private StyleRepository styleRepository;
    private CountryRepository countryRepository;

    public BeerServiceImpl(BeerRepository beerRepository, TagRepository tagRepository,StyleRepository styleRepository, CountryRepository countryRepository) {
        this.beerRepository = beerRepository;
        this.tagRepository = tagRepository;
        this.styleRepository = styleRepository;
        this.countryRepository = countryRepository;
    }

    @Override
    public List<Beer> getAll() {
        return beerRepository.getAll();
    }

    @Override
    public void createBeer(Beer beer){
        beerRepository.createBeer(beer);
    }

    @Override
    public void updateBeer(int id, Beer beer) {
        beerRepository.updateBeer(id, beer);
    }

    @Override
    public Beer getByName(String name) {
        return beerRepository.getByName(name);
    }

    @Override
    public Beer getById(int id) {
        return beerRepository.getById(id);
    }

    @Override
    public void addTagToBeer(String tagName, int beerId) {
        try {
            Tag tag = tagRepository.getTagByName(tagName);
            if (tag == null) {
                Tag newTag = new Tag(tagName);
                tagRepository.create(newTag);
            }
            int tagId = tagRepository.getTagByName(tagName).getId();
            if (beerRepository.getBeerTags(beerId).stream().filter(p -> p.getId() == tagId).findAny().orElse(null) == null){
                beerRepository.addTagToBeer(tagId, beerId);
            }
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public List<Beer> getAllFilteredByCountry(String countryName) {
        try {
            List<Beer> countryBeers = beerRepository.getAll()
                    .stream()
                    .filter(p ->p.getCountry().getCountry().equals(countryName))
                    .collect(Collectors.toList());
            return countryBeers;
        } catch (Exception e) {
            throw new IllegalArgumentException(
                    String.format("A country with name %s doesn't exist.", countryName)
            );
        }
    }

    @Override
    public List<Beer> getAllFilteredByStyle(String styleName) {
        try {
            return beerRepository.getAll().stream().filter(p -> p.getStyle().getStyle().equals(styleName)).collect(Collectors.toList());
        } catch (Exception e) {
            throw new IllegalArgumentException(
                    String.format("A style with name %s doesn't exist.", styleName)
            );
        }
    }

    @Override
    public List<Beer> getAllFilteredByTag(String tagName) {
        try {
            return tagRepository.getTagBeers(tagName);
        } catch (Exception e) {
            throw new IllegalArgumentException(
                    String.format("A tag with name %s doesn't exist.", tagName)
            );
        }
    }

    @Override
    public void deleteBeer(int id) {
        beerRepository.deleteBeer(id);
    }

    @Override
    public List<Beer> sortAlphabetically() {
        return beerRepository.sortAlphabetically();
    }

    @Override
    public List<Beer> sortByABV() {
        return beerRepository.sortedByABV();
    }

    @Override
    public List<Beer> sortByRating() {
        return beerRepository.sortedByRating();
    }

    @Override
    public List<Country> countries() {
        return beerRepository.countries();
    }

    @Override
    public List<Tag> getBeerTags(int id){
        return beerRepository.getBeerTags(id);
    }

    @Override
    public List<Rating> getBeerRatingList(int id){
        return beerRepository.getBeerRatingList(id);
    }
}
