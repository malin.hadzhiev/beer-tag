package com.module3.beertag.services;

import com.module3.beertag.models.Style;
import com.module3.beertag.repositories.contracts.StyleRepository;
import com.module3.beertag.services.contracts.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StyleServiceImpl implements StyleService {
    private StyleRepository styleRepository;

    @Autowired
    public StyleServiceImpl(StyleRepository styleRepository) {
        this.styleRepository = styleRepository;
    }

    @Override
    public List<Style> getAll() {
        return styleRepository.getAll();
    }
}
