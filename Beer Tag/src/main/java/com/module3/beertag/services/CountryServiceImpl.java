package com.module3.beertag.services;

import com.module3.beertag.models.Brewery;
import com.module3.beertag.models.Country;
import com.module3.beertag.repositories.contracts.CountryRepository;
import com.module3.beertag.services.contracts.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {
    private CountryRepository countryRepository;

    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public List<Country> getAll() {
        return countryRepository.getAll();
    }
}
