package com.module3.beertag.services.contracts;

import com.module3.beertag.models.Style;

import java.util.List;

public interface StyleService {
    List<Style> getAll();
}
