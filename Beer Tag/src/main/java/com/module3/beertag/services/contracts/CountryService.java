package com.module3.beertag.services.contracts;

import com.module3.beertag.models.Country;

import java.util.List;

public interface CountryService {
    List<Country> getAll();
}
