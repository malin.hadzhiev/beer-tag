package com.module3.beertag.repositories;

import com.module3.beertag.models.Rating;
import com.module3.beertag.repositories.contracts.RatingRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RatingRepositoryImpl implements RatingRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public RatingRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Rating rating){
        try (Session session = sessionFactory.openSession()) {
            session.save(rating);
        }
    }

    @Override
    public List<Rating> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Rating> query = session.createQuery("from Rating", Rating.class);
            return query.list();
        }
    }

    @Override
    public Rating getRatingById(int id) {
        Rating rating;
        try (Session session = sessionFactory.openSession()) {
            rating = session.get(Rating.class, id);
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }

        if (rating == null) {
            throw new IllegalArgumentException("This rating does not exist in the database");
        }
        return rating;
    }
    @Override
    public Rating getRatingByUserIdAndBeerId(int userId, int beerId) {
        try (Session session = sessionFactory.openSession()){
            Query<Rating> query = session.createQuery("from Rating where beer_id = :beerid and user_id = :userid",Rating.class);
            query.setParameter("userid", userId);
            query.setParameter("beerid", beerId);
            return query.list().stream().findFirst().orElse(null);
        }
    }
}
