package com.module3.beertag.repositories.contracts;

import com.module3.beertag.models.Brewery;

import java.util.List;

public interface BreweryRepository {
    void create(Brewery brewery);

    Brewery getBreweryById(int id);

    List<Brewery> getAll();

    void updateBrewery(int id, Brewery brewery);

    void deleteBrewery(int id);

    Brewery getBreweryByName(String name);
}
