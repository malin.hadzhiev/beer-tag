package com.module3.beertag.repositories;

import com.module3.beertag.models.Beer;
import com.module3.beertag.models.Rating;
import com.module3.beertag.models.User;
import com.module3.beertag.models.BeerStatus;
import com.module3.beertag.repositories.contracts.UserRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
@Repository
public class UserRepositoryImpl implements UserRepository {
    private SessionFactory sessionFactory;

    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User", User.class);
            return query.list();
        }
    }

    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.saveOrUpdate(user);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }
    
    @Override
    public List<Rating> getUserRatedBeers(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            return new ArrayList<>(user.getRatedBeers());
        }
    }
    @Override
    public List<Beer> getUserCreatedBeers(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            return new ArrayList<>(user.getCreatedBeers());
        }
    }
    @Override
    public List<BeerStatus> getUserBeerStatus(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            return new ArrayList<>(user.getBeerStatusList());
        }
    }
    @Override
    public void addToDrankList(int userid, int beerid) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                Query query = session.createSQLQuery("insert into users_to_beers (user_id, beer_id,is_drunk)" +
                        "values (:userid, :beerid,1)");
                query.setParameter("userid", userid);
                query.setParameter("beerid", beerid);
                query.executeUpdate();
                session.getTransaction().commit();
            } catch (HibernateException e) {
                session.getTransaction().rollback();
            }
        }
    }
    @Override
    public void addToWishList(int userid, int beerid) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                Query query = session.createSQLQuery("insert into users_to_beers (user_id, beer_id,is_drunk)" +
                        "values (:userid, :beerid,0)");
                query.setParameter("userid", userid);
                query.setParameter("beerid", beerid);
                query.executeUpdate();
                session.getTransaction().commit();
            } catch (HibernateException e) {
                session.getTransaction().rollback();
            }
        }
    }

    @Override
    public void updateToDrankList(int userid, int beerid,int statusID) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                Query query = session.createSQLQuery("update users_to_beers set beer_id = :beerid, user_id = :userid, is_drunk = 1 where id = :statusID");
                query.setParameter("userid", userid);
                query.setParameter("beerid", beerid);
                query.setParameter("statusID", statusID);
                query.executeUpdate();
                session.getTransaction().commit();
            } catch (HibernateException e) {
                session.getTransaction().rollback();
            }
        }
    }



    @Override
    public User getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username", username);
            return query.list().stream().findFirst().orElse(null);
        }
    }

    @Override
    public void delete(User user) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.update(user);
        session.getTransaction().commit();
    }
    @Override
    public void addRating(int rating, int userid, int beerid){
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                Query query = session.createSQLQuery("insert into ratings set beer_id = :beerid, user_id = :userid, rating = :rating");
                query.setParameter("userid", userid);
                query.setParameter("beerid", beerid);
                query.setParameter("rating", rating);
                query.executeUpdate();
                session.getTransaction().commit();
            } catch (HibernateException e) {
                session.getTransaction().rollback();
            }
        }
    }
    @Override
    public void editRating(int rating, int userid, int beerid){
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                Query query = session.createSQLQuery("update ratings set rating = :rating where user_id = :userid and beer_id = :beerid");
                query.setParameter("userid", userid);
                query.setParameter("beerid", beerid);
                query.setParameter("rating", rating);
                query.executeUpdate();
                session.getTransaction().commit();
            } catch (HibernateException e) {
                session.getTransaction().rollback();
            }
        }
    }

}
