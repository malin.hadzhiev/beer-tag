package com.module3.beertag.repositories.contracts;

import com.module3.beertag.models.Beer;
import com.module3.beertag.models.Country;
import com.module3.beertag.models.Rating;
import com.module3.beertag.models.Tag;

import java.util.List;

public interface BeerRepository {
    List<Beer> getAll();

    void createBeer(Beer beer);

    List<Tag> getBeerTags(int id);

    List<Rating> getBeerRatingList(int id);

    Beer getById(int id);

    void addTagToBeer(int tagId, int beerId);


    List<Beer> sortAlphabetically();

    List<Beer> sortedByABV();

    List<Beer> sortedByRating();

    List<Country> countries();

    Beer getByName(String name);

    void updateBeer(int id, Beer beer);

    void deleteBeer(int id);

    void updateAverageRating(int beerid, double averageRating);

}
