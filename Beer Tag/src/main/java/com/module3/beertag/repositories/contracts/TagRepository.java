package com.module3.beertag.repositories.contracts;

import com.module3.beertag.models.Beer;
import com.module3.beertag.models.Tag;

import java.util.List;

public interface TagRepository {
    void create(Tag tag);

    void deleteTag(int id);

    void updateTag(int id, Tag tag);

    List<Tag> getAll();

    Tag getTagById(int id);

    List<Beer> getTagBeers(String tagName);

    Tag getTagByName(String name);
}
