package com.module3.beertag.repositories;

import com.module3.beertag.models.Beer;
import com.module3.beertag.models.Brewery;
import com.module3.beertag.models.Tag;
import com.module3.beertag.repositories.contracts.TagRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class TagRepositoryImpl implements TagRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public TagRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Tag tag) {
        try (Session session = sessionFactory.openSession()) {
            session.save(tag);
        }
    }

    @Override
    public void deleteTag(int id) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                Query queryBeersToTags = session.createSQLQuery("delete from beers_to_tags where tag_id = :id");
                queryBeersToTags.setParameter("id", id);
                queryBeersToTags.executeUpdate();
                Query queryBeers = session.createSQLQuery("delete from tags where id = :id");
                queryBeers.setParameter("id", id);
                queryBeers.executeUpdate();
                session.getTransaction().commit();
            } catch (HibernateException e) {
                session.getTransaction().rollback();
            }
        }
    }


    @Override
    public void updateTag(int id, Tag tag) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                Query query = session.createSQLQuery(
                        "update tags set name = :name where id = :id");

                query.setParameter("id", id);
                query.setParameter("name", tag.getTag());

                query.executeUpdate();
                session.getTransaction().commit();
            } catch (HibernateException e) {
                session.getTransaction().rollback();
            }
        }
    }

    @Override
    public List<Tag> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Tag",Tag.class).list();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public Tag getTagById(int id) {
        Tag tag;
        try (Session session = sessionFactory.openSession()) {
            tag = session.get(Tag.class, id);
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }

        if (tag == null) {
            throw new IllegalArgumentException("This tag does not exist in the database");
        }
        return tag;
    }


    @Override
    public List<Beer> getTagBeers(String tagName) {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> tag = session
                    .createQuery("from Tag where name = :tagName", Tag.class);
            tag.setParameter("tagName", tagName);
            if (tag.list().isEmpty()){
                return new ArrayList<>();
            }
            return new ArrayList<>(tag.list().get(0).getBeers());
        }
    }

    @Override
    public Tag getTagByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery("from Tag where name = :tagName", Tag.class);
            query.setParameter("tagName", name);
            if (query.list().isEmpty()) {
                return null;
            }
            return (Tag) query.list().get(0);
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }


}
