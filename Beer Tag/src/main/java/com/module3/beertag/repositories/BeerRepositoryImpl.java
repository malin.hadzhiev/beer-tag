package com.module3.beertag.repositories;

import com.module3.beertag.models.Beer;
import com.module3.beertag.models.Country;
import com.module3.beertag.models.Rating;
import com.module3.beertag.models.Tag;
import com.module3.beertag.repositories.contracts.BeerRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class BeerRepositoryImpl implements BeerRepository {
    private SessionFactory sessionFactory;

    public BeerRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Beer> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer", Beer.class);
            return query.list();
        }
    }

    @Override
    public void createBeer(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            session.save(beer);
        }
    }

    @Override
    public void updateBeer(int id, Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                Query query = session.createSQLQuery("update beers set name = :beername " +
                        ", ABV = :abv, description = :beerdescription " +
                        ",brewery_id = :brewery, country_id = :country," +
                        " style_id = :style where id = :id");

                query.setParameter("id", id);
                query.setParameter("beername", beer.getName());
                query.setParameter("brewery", beer.getBrewery().getId());
                query.setParameter("country", beer.getCountry().getId());
                query.setParameter("abv", beer.getABV());
                query.setParameter("beerdescription", beer.getDescription());
                query.setParameter("style", beer.getStyle().getId());
                query.executeUpdate();
                session.getTransaction().commit();
            } catch (HibernateException e) {
                session.getTransaction().rollback();
            }
        }
    }

    @Override
    public List<Tag> getBeerTags(int id) {
        try (Session session = sessionFactory.openSession()) {
            Beer beer = session.get(Beer.class, id);
            return new ArrayList<>(beer.getTags());
        }

    }

    @Override
    public List<Rating> getBeerRatingList(int id) {
        try (Session session = sessionFactory.openSession()) {
            Beer beer = session.get(Beer.class, id);
            return new ArrayList<>(beer.getRatingList());
        }
    }

    @Override
    public void addTagToBeer(int tagId, int beerId) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                Query query = session.createSQLQuery("insert into beers_to_tags (tag_id, beer_id)" +
                        "values (:tagid, :beerid)");
                query.setParameter("tagid", tagId);
                query.setParameter("beerid", beerId);
                query.executeUpdate();
                session.getTransaction().commit();
            } catch (HibernateException e) {
                session.getTransaction().rollback();
            }
        }
    }

    @Override
    public Beer getById(int id) {
        Beer beer;

        try (Session session = sessionFactory.openSession()) {
            beer = session.get(Beer.class, id);
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }

        if (beer == null) {
            throw new IllegalArgumentException(
                    String.format("Beer with id %d does not exist", id));
        }
        return beer;
    }

    @Override
    public Beer getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery("from Beer where name = :name");
            query.setParameter("name", name);
            if (query.list().isEmpty()) {
                return null;
            }
            return (Beer) query.list().get(0);
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public List<Beer> sortAlphabetically() {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer order by name", Beer.class);
            return query.list();
        }
    }

    @Override
    public List<Beer> sortedByABV() {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer order by ABV desc, name asc", Beer.class);
            return query.list();
        }
    }

    @Override
    public List<Beer> sortedByRating() {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer order by average_rating desc, name asc", Beer.class);
            return query.list();
        }
    }

    @Override
    public List<Country> countries() {
        Session session = sessionFactory.openSession();
        Query<Country> query = session.createQuery("from Country", Country.class);
        return query.list();
    }

    @Override
    public void deleteBeer(int id) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                Query queryBeersToTags = session.createSQLQuery("delete from beers_to_tags where beer_id = :id");
                queryBeersToTags.setParameter("id", id);
                queryBeersToTags.executeUpdate();
                Query queryBeersToRating = session.createSQLQuery("delete from ratings where beer_id = :id");
                queryBeersToRating.setParameter("id", id);
                queryBeersToRating.executeUpdate();
                Query queryBeersToUsers = session.createSQLQuery("delete from users_to_beers where beer_id = :id");
                queryBeersToUsers.setParameter("id", id);
                queryBeersToUsers.executeUpdate();
                Query queryBeers = session.createSQLQuery("delete from beers where id = :id");
                queryBeers.setParameter("id", id);
                queryBeers.executeUpdate();
                session.getTransaction().commit();
            } catch (HibernateException e) {
                session.getTransaction().rollback();
            }
        }

    }
    @Override
    public void updateAverageRating(int beerid, double averageRating) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                Query query = session.createSQLQuery("update beers set average_rating = :averageRating where id = :beerid");
                query.setParameter("averageRating", averageRating);
                query.setParameter("beerid", beerid);
                query.executeUpdate();
                session.getTransaction().commit();
            } catch (HibernateException e) {
                session.getTransaction().rollback();
            }
        }

    }

}