package com.module3.beertag.repositories.contracts;

import com.module3.beertag.models.Beer;
import com.module3.beertag.models.Rating;
import com.module3.beertag.models.User;
import com.module3.beertag.models.BeerStatus;

import java.util.List;

public interface UserRepository  {
    List<User> getAll();

    List<Rating> getUserRatedBeers(int id);

    List<Beer> getUserCreatedBeers(int id);

    List<BeerStatus> getUserBeerStatus(int id);

    void create(User user);

    User getByUsername(String username);

    void delete(User user);

    void addToDrankList(int userid, int beerid);

    void addToWishList(int userid, int beerid);

    void updateToDrankList(int userid, int beerid,int statusID);

    void addRating(int rating, int userid, int beerid);

    void editRating(int rating, int userid, int beerid);


}
