package com.module3.beertag.repositories.contracts;

import com.module3.beertag.models.Style;

import java.util.List;

public interface StyleRepository {
    List<Style> getAll();

    Style getStyleByName(String name);
}
