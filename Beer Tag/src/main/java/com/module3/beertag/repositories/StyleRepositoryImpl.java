package com.module3.beertag.repositories;

import com.module3.beertag.models.Style;
import com.module3.beertag.repositories.contracts.StyleRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StyleRepositoryImpl implements StyleRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public StyleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Style> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Style> query = session.createQuery("from Style", Style.class);
            return query.list();
        }
    }
    @Override
    public Style getStyleByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Style> query = session.createQuery("from Style where name = :name",Style.class);
            query.setParameter("name", name);

            return query.list().stream().findFirst().orElse(null);
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

}
