package com.module3.beertag.repositories.contracts;

import com.module3.beertag.models.Country;

import java.util.List;

public interface CountryRepository {
    List<Country> getAll();

    Country getCountryByName(String name);
}
