package com.module3.beertag.repositories;

import com.module3.beertag.models.Brewery;
import com.module3.beertag.repositories.contracts.BreweryRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BreweryRepositoryImpl implements BreweryRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public BreweryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Brewery brewery){
        try (Session session = sessionFactory.openSession()) {
            session.save(brewery);
        }
    }

    @Override
    public Brewery getBreweryById(int id) {
        try (Session session = sessionFactory.openSession()){
            return session.get(Brewery.class, id);
        }
    }

    @Override
    public List<Brewery> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Brewery> query = session.createQuery("from Brewery", Brewery.class);
            return query.list();
        }
    }

    @Override
    public void updateBrewery(int id, Brewery brewery) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                Query query = session.createSQLQuery(
                        "update breweries set name = :name where id = :id");

                query.setParameter("id", id);
                query.setParameter("name", brewery.getBrewery());
                query.executeUpdate();
                session.getTransaction().commit();
            } catch (HibernateException e) {
                session.getTransaction().rollback();
            }
        }
    }

    @Override
    public void deleteBrewery(int id) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                Query queryBeers = session.createSQLQuery("delete from beers where brewery_id = :id");
                queryBeers.setParameter("id", id);
                queryBeers.executeUpdate();
                Query queryBreweries = session.createSQLQuery("delete from breweries where id = :id");
                queryBreweries.setParameter("id", id);
                queryBreweries.executeUpdate();
                session.getTransaction().commit();
            } catch (HibernateException e) {
                session.getTransaction().rollback();
            }
        }

    }

    @Override
    public Brewery getBreweryByName(String name) {
        Brewery brewery;
        try (Session session = sessionFactory.openSession()) {
            Query<Brewery> query = session.createQuery("from Brewery" +
                    " where name = :name", Brewery.class);
            query.setParameter("name", name);

            brewery = query.getSingleResult();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
        return brewery;
    }
}
