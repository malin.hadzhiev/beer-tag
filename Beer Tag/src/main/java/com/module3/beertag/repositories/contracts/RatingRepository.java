package com.module3.beertag.repositories.contracts;

import com.module3.beertag.models.Rating;

import java.util.List;

public interface RatingRepository {
    List<Rating> getAll();

    Rating getRatingById(int id);

    void create(Rating rating);

    Rating getRatingByUserIdAndBeerId(int userid, int beerid);
}
