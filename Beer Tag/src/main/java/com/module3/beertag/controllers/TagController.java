package com.module3.beertag.controllers;

import com.module3.beertag.models.Brewery;
import com.module3.beertag.models.Tag;
import com.module3.beertag.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/tags")
public class TagController {
    private TagService tagService;

    @Autowired
    public TagController(TagService tagService){
        this.tagService = tagService;
    }

    @GetMapping("/new")
    public String getNewTagView(Model model) {
        model.addAttribute("tag", new Tag());
        return "newTag";
    }

    @PostMapping("/new")
    public String newTag(@Valid @ModelAttribute Tag tag,
                          BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "newTag";
        }
        tagService.createTag(tag);

        return "redirect:/beers";
    }

    @GetMapping("/{id}/delete")
    public String deleteTag(@PathVariable int id) {
        tagService.deleteTag(id);

        return "redirect:/beers";
    }

    @GetMapping("/{id}/edit")
    public String getUpdateTag(Model model, @PathVariable int id) {
        model.addAttribute("tag", tagService.getTagById(id));

        return "editTags";
    }

    @PutMapping("/{id}/edit")
    public String updateTag(@ModelAttribute Tag tag, @PathVariable int id) {
        tagService.updateTag(id, tag);

        return "redirect:/editTags";
    }
}
