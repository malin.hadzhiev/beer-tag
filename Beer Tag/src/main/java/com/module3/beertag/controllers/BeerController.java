package com.module3.beertag.controllers;

import com.module3.beertag.models.*;
import com.module3.beertag.services.contracts.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.nio.file.attribute.UserPrincipal;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/beers")
public class BeerController {
    private BeerService beerService;
    private CountryService countryService;
    private BreweryService breweryService;
    private StyleService styleService;
    private UserService userService;

    @Autowired
    public BeerController(BeerService beerService, CountryService countryService, BreweryService breweryService, StyleService styleService, UserService userService) {
        this.beerService = beerService;
        this.countryService = countryService;
        this.breweryService = breweryService;
        this.styleService = styleService;
        this.userService = userService;
    }



    @GetMapping
    public String getAllBeersPage(Model model,Principal principal) {
        model.addAttribute("beersList", beerService.getAll());
        if (principal != null){
            model.addAttribute("currentUserID",userService.getUserByName(principal.getName()).getId());
        }
        return "allBeers";
    }

    @GetMapping("/filterBy")
    public String getAllBeersPage(Model model,
                                  Principal principal,
                                  @RequestParam(required = false) String country,
                                  @RequestParam(required = false) String style,
                                  @RequestParam(required = false) String tag) {
        List<Beer> beers;
        if (country != null) {
            beers = beerService.getAllFilteredByCountry(country);
        } else if (style != null) {
            beers = beerService.getAllFilteredByStyle(style);
        } else if (tag != null) {
            beers = beerService.getAllFilteredByTag(tag);
        } else {
            beers = beerService.getAll();
        }

        model.addAttribute("beersList", beers);
        if (principal != null){
            model.addAttribute("currentUserID",userService.getUserByName(principal.getName()).getId());
        }
        return "allBeers";
    }

    @GetMapping("/{id}")
    public String getBeerTags(Model model,@PathVariable int id,Principal principal) {
        model.addAttribute("tags", beerService.getBeerTags(id));
        model.addAttribute("beer", beerService.getById(id));
        model.addAttribute("ratingList", beerService.getBeerRatingList(id));
        model.addAttribute("status", userService.getUserBeerStatus(userService.
                getUserByName(principal.getName())
                .getId())
                .stream()
                .filter(p -> p.getBeer().getId() == id).findFirst().orElse(null));
        return "beerDetails";
    }

    @GetMapping("/{id}/edit")
    public String getUpdateBeerView(Model model,@PathVariable int id) {
        model.addAttribute("beer", beerService.getById(id));
        model.addAttribute("countries", countryService.getAll());
        model.addAttribute("breweries",breweryService.getAll());
        model.addAttribute("styles",styleService.getAll());
        return "updateBeer";
    }

    @PutMapping("/{id}/edit")
    public String updateBeer(@ModelAttribute Beer beer,
                          @ModelAttribute("country") String country,
                          @ModelAttribute("brewery") String brewery,
                          @ModelAttribute("style") String style,
                          Principal principal,
                          @PathVariable int id) {
        Beer beerToBeUpdated = beerService.getById(id);
        if (!beerToBeUpdated.getCreator().getUsername().equals(principal.getName())){
            throw new IllegalArgumentException();
        }
        beer.setCreator(userService.getUserByName(principal.getName()));
        beerService.updateBeer(id, beer);

        return "redirect:/beers";
    }

    @GetMapping("/new")
    public String getNewBeerView(Model model) {
        model.addAttribute("beer", new Beer());
        model.addAttribute("countries", countryService.getAll());
        model.addAttribute("breweries",breweryService.getAll());
        model.addAttribute("styles",styleService.getAll());
        return "newBeer";
    }

    @PostMapping("/new")
    public String newBeer(@Valid @ModelAttribute Beer beer,
                          @ModelAttribute("country") String country,
                          @ModelAttribute("brewery") String brewery,
                          @ModelAttribute("style") String style,
                          BindingResult bindingResult,
                          Principal principal) {
        if (bindingResult.hasErrors()) {
            return "newBeer";
        }
        User creator = userService.getUserByName(principal.getName());
        beer.setCreator(creator);
        beerService.createBeer(beer);

        return "redirect:/beers";
    }

    @GetMapping("/{beerId}/add-tag")
    public String addTagToBeer(@RequestParam String tag, @PathVariable int beerId) {
        beerService.addTagToBeer(tag, beerId);

        return "redirect:/beers";
    }

    @GetMapping("/sorted/alphabetically")
    public String sortedAlphabetically(Model model, Principal principal) {
        model.addAttribute("beersList", beerService.sortAlphabetically());
        if (principal != null){
            model.addAttribute("currentUserID",userService.getUserByName(principal.getName()).getId());
        }
        return "allBeers";
    }

    @GetMapping("/sorted/by/ABV")
    public String sortedByABV(Model model, Principal principal) {
        model.addAttribute("beersList", beerService.sortByABV());
        if (principal != null){
            model.addAttribute("currentUserID",userService.getUserByName(principal.getName()).getId());
        }
        return "allBeers";
    }

    @GetMapping("/sorted/by/rating")
    public String sortedByRating(Model model, Principal principal) {
        model.addAttribute("beersList", beerService.sortByRating());
        if (principal != null){
            model.addAttribute("currentUserID",userService.getUserByName(principal.getName()).getId());
        }
        return "allBeers";
    }

    @GetMapping("/{id}/delete")
    public String deleteBeer(@PathVariable int id) {
        beerService.deleteBeer(id);
        return "redirect:/beers";
    }
}


