package com.module3.beertag.controllers;

import com.module3.beertag.services.contracts.BreweryService;
import com.module3.beertag.services.contracts.TagService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AdminController {

    private BreweryService breweryService;
    private TagService tagService;

    public AdminController(BreweryService breweryService, TagService tagService) {
        this.breweryService = breweryService;
        this.tagService = tagService;
    }

    @GetMapping("/admin")
    public String showAdminPanel(Model model) {
        model.addAttribute("breweries", breweryService.getAll());
        model.addAttribute("tags", tagService.getAll());
        return "admin";
    }
}
