package com.module3.beertag.controllers;

import com.module3.beertag.models.Beer;
import com.module3.beertag.models.User;
import com.module3.beertag.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequestMapping("/users")
public class UserController {
    private UserService service;

    @Autowired
    public UserController(UserService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public String getUserRatedBeers(Model model, @PathVariable int id, Principal principal) {
        if (service.getUserByName(principal.getName()).getId() != id){
            return "access-denied";
        }
        model.addAttribute("ratingList", service.getUserRatedBeers(id));
        model.addAttribute("createdList", service.getUserCreatedBeers(id));
        model.addAttribute("beerDrunkStatus", service.getUserDrunkStatus(id));
        model.addAttribute("beerWantToDrinkStatus",service.getUserWantToDrinkStatus(id));
        model.addAttribute("currentUser",service.getUserByName(principal.getName()));
        model.addAttribute("topRatedBeers",service.getUserTopRatedBeers(id));
        return "UserDetails";
    }

    @GetMapping("/addToDrankList/{beerId}")
    public String addToDrankList(@PathVariable int beerId, Principal principal) {
        int userId = service.getUserByName(principal.getName()).getId();
        service.addToDrankList(userId, beerId);
        return "cheers";
    }
    @GetMapping("/addToWishList/{beerId}")
    public String addToWishList(@PathVariable int beerId, Principal principal) {
        int userId = service.getUserByName(principal.getName()).getId();
        service.addToWishList(userId, beerId);
        return "redirect:/beers";
    }


    @PostMapping("/createUser")
    public void createUser(@RequestBody User user) {
        try {
            service.create(user);
        } catch (Throwable e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
    @PostMapping("/addRating/{beerid}")
    public String addRating(@PathVariable int beerid, Principal principal,
                            @RequestParam int rating) {
        String name = principal.getName();
        int userid = service.getUserByName(name).getId();
        service.addRating(rating, userid, beerid);


        return "redirect:/beers";
    }

}
