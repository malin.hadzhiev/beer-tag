package com.module3.beertag.controllers.REST;

import com.module3.beertag.models.Beer;
import com.module3.beertag.services.contracts.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/beers")
public class BeerRestController {
    private BeerService beerService;

    @Autowired
    public BeerRestController(BeerService beerService) {
        this.beerService = beerService;
    }

    @GetMapping
    public List<Beer> getAll() {
        return beerService.getAll();
    }

    @GetMapping("/{id}")
    public Beer getById(@PathVariable int id) {
        try {
            return beerService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/name-{name}")
    public Beer getByName(@PathVariable String name) {
        try {
            return beerService.getByName(name);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/sortByRating")
    public List<Beer> sortByRating() {
        try {
            return beerService.sortByRating();
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/sortByAlphabetically")
    public List<Beer> sortByAlphabetically() {
        try {
            return beerService.sortAlphabetically();
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/sortByAbv")
    public List<Beer> sortByAbv() {
        try {
            return beerService.sortByABV();
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filter-by-style-{style}")
    public List<Beer> filterByStyle(@PathVariable String style) {
        try {
            return beerService.getAllFilteredByStyle(style);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filter-by-country-{country}")
    public List<Beer> filterByCountry(@PathVariable String country) {
        try {
            return beerService.getAllFilteredByCountry(country);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filter-by-tag-{tag}")
    public List<Beer> filterByTag(@PathVariable String tag) {
        try {
            return beerService.getAllFilteredByTag(tag);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/tag{tag}-{beerId}")
    public String addTagToBeer(@PathVariable String tag, @PathVariable int beerId) {
        try {
            beerService.addTagToBeer(tag, beerId);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return "Tag added successful";
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable int id) {
        try {
            beerService.deleteBeer(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/createBeer")
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody Beer createBeer(@Valid @RequestBody Beer beer) {
        try {
            beerService.createBeer(beer);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return beer;
    }
}
