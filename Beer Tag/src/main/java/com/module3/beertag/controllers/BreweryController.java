package com.module3.beertag.controllers;

import com.module3.beertag.models.Brewery;
import com.module3.beertag.services.contracts.BreweryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/breweries")
public class BreweryController {
    private BreweryService breweryService;

    @Autowired
    public BreweryController(BreweryService breweryService) {
        this.breweryService = breweryService;
    }

    @GetMapping("/new")
    public String getNewBreweryView(Model model) {
        model.addAttribute("brewery", new Brewery());
        return "newBrewery";
    }

    @PostMapping("/new")
    public String newBrewery(@Valid @ModelAttribute Brewery brewery,
                             BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "newBrewery";
        }
        breweryService.createBrewery(brewery);

        return "redirect:/beers";
    }

    @GetMapping("/{id}/delete")
    public String deleteBrewery(@PathVariable int id) {
        breweryService.deleteBrewery(id);
        return "redirect:/beers";
    }

    @GetMapping("/{id}/edit")
    public String getUpdateBrewery(Model model, @PathVariable int id) {
        model.addAttribute("brewery", breweryService.getBreweryById(id));

        return "editBrewery";
    }
    @PutMapping("/{id}/edit")
    public String updateBrewery(@ModelAttribute Brewery brewery, @PathVariable int id) {
        breweryService.updateBrewery(id, brewery);

        return "redirect:/editBrewery";
    }
}
