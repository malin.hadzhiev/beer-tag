package com.module3.beertag.controllers.REST;

import com.module3.beertag.models.User;
import com.module3.beertag.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserRestController {
    private UserService userService;

    @Autowired
    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<User> getAll() {
        return userService.getAll();
    }

    @GetMapping("/{name}")
    public User getUserByName(@PathVariable String name) {
        try {
            return userService.getUserByName(name);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/createUser")
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody User createUser(@Valid @RequestBody User user) {
        try {
            userService.create(user);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return user;
    }

    @PutMapping("/drank-list{userId}-{beerId}")
    public String addToDrankList(@PathVariable int userId, @PathVariable int beerId) {
        try {
            userService.addToDrankList(userId, beerId);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return "Beer Added to Drink List Successful";
    }

    @PutMapping("/wish-list{userId}-{beerId}")
    public String addToWishList(@PathVariable int userId, @PathVariable int beerId) {
        try {
            userService.addToWishList(userId, beerId);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return "Beer Added to Wish List Successful";
    }

    @PutMapping("/rating{rating}-{userId}-{beerId}")
    public String addRating(@PathVariable int rating ,@PathVariable int userId, @PathVariable int beerId) {
        try {
            userService.addRating(rating, userId, beerId);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return "Rating added successful";
    }
}
